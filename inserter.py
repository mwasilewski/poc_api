import sys
import json
import requests


URL = "http://127.0.0.1:5000/stock"


def read(filename):
    # tag
    # name, data, price1, price2, price3, price4, volume
    items = []
    with open(filename, 'r') as fd:
        for line in fd:
            name, data, price1, price2, price3, price4, volume = line.split(",")

            items.append({
                "name": name,
                "buy_time": data,
                "price1": price1,
                "price2": price2,
                "price3": price3,
                "price4": price4,
                "volume": volume
            })
    return items


def send(items):
    message = json.dumps(items)
    headers = {"Content-Type": "application/json"}
    requests.post(URL, headers=headers, data=message)


if __name__ == "__main__":
    items = read(sys.argv[1])
    send(items)
