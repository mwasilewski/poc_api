from flask import Flask
from flask import request
from flask import abort
from flask_mysqldb import MySQL
import json
import zmq
import uuid
import datetime

# Config
# Choose from '*', '127.0.0.1'
API_HOST = '*'
API_PORT = 6000
DB_HOST = '172.17.0.2'  # 'db'
DB_USER = 'root'
DB_PASS = 'somepassword'
DB_NAME = 'poc'


def init_db(app):
    app.config['MYSQL_HOST'] = DB_HOST
    app.config['MYSQL_USER'] = DB_USER
    app.config['MYSQL_PASSWORD'] = DB_PASS
    app.config['MYSQL_DB'] = DB_NAME
    mysql_ob = MySQL(app)
    return mysql_ob


def init_app():
    app = Flask(__name__)
    mysql_ob = init_db(app)
    return app, mysql_ob


app, mysql = init_app()


def get_db():
    global mysql
    conn = mysql.connection
    return conn.cursor()


def commit():
    global mysql
    conn = mysql.connection
    conn.commit()


def send_to_queue(new_id):
    """
    Uses ZeroMQ.
    """
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.bind("tcp://{}:{}".format(API_HOST, API_PORT))
    work_message = {"id": new_id}
    zmq_socket.send_json(work_message)


@app.route('/init_db')
def init_db():
    # TODO: should be moved to script which would run this in app context
    create_numbers = """
                     CREATE TABLE IF NOT EXISTS numbers
                     (id varchar(36) primary key, input int, result int);
                     """
    create_stock = """
                   CREATE TABLE IF NOT EXISTS stock
                   (
                    id varchar(36) primary key,
                    name varchar(72),
                    buy_time date,
                    price1 float,
                    price2 float,
                    price3 float,
                    price4 float,
                    volume int,
                    inserted_time datetime
                   );
                   """
    db = get_db()
    db.execute(create_numbers)
    db.execute(create_stock)
    commit()
    return 'Init completed'


@app.route('/stock', methods=['GET', 'POST'])
def stock_data():
    """
    Works with bossa format.
    """
    if not request.json:
        abort(400, "not json")
    data = request.json
    db = get_db()
    inserted_time = datetime.datetime.now()
    inserted_time = inserted_time.strftime('%Y-%m-%d %H:%M:%S')
    for item in data:
        buy_time = "{}-{}-{}".format(
            item["buy_time"][:4],
            item["buy_time"][4:6],
            item["buy_time"][6:]
        )
        command = """
            INSERT INTO stock(
                id, name, buy_time,
                price1, price2, price3,
                price4, volume, inserted_time)
            VALUES
                ('{}', '{}', '{}', {}, {}, {}, {}, {}, '{}');
        """.format(
            str(uuid.uuid4()), item["name"], buy_time,
            item["price1"], item["price2"], item["price3"],
            item["price4"], item["volume"], inserted_time
        )
        db.execute(command)
    commit()
    return json.dumps({"status": "ok"})


@app.route('/')
def index():
    return 'Simple processing system'


@app.route('/numbers', methods=['POST'])
def numbers():
    if not request.json:
        abort(400, "not json")
    data = request.json
    if 'number' not in data:
        abort(400, 'number not in data')

    new_id = str(uuid.uuid4())
    db = get_db()
    command = "INSERT INTO numbers (id, input) VALUES ('{}',{})".format(
        new_id, int(data['number'])
    )
    db.execute(command)
    commit()
    send_to_queue(new_id)
    return json.dumps({'id': new_id})
