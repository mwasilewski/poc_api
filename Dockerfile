FROM ubuntu:16.04
COPY poc_api /opt/poc_api

RUN apt-get -y update
RUN apt-get -y install libmysqlclient-dev python-pip
RUN pip install -r /opt/poc_api/requirements.txt
ENV FLASK_APP /opt/poc_api/api.py
CMD [ "flask", "run", "--host=0.0.0.0" ]
